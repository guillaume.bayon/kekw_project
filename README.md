# Enoncer du TP (en binôme)
jeej
## Pré-requis

## Ce TP utilise

- [GIT](https://git-scm.com/downloads)
- [JDK11](https://www.oracle.com/fr/java/technologies/javase-jdk11-downloads.html#license-lightbox) 
- [Maven 3.6.x](http://apache.mirrors.nublue.co.uk/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip)

## Astuces

- Gitlab inject des variables disponibles dans la CI, vous trouverez la liste [ici](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- Vous pouvez aussi injecter vos propres variables dans **Settings > CICD**

## Préparation

1. Une personne par groupe doit récupérer le projet dans son espace personnel Gitlab
2. Cette personne doit ajouter son binôme en tant que developer

## Partie 1 - Configuration du repository

1. Configurez le repository pour que personne ne puisse push directement sur la branche master
2. Configurez le repository pour qu'il y ait au moins 1 approver sur une merge request
3. Pour tester, créez une issue dans gitlab et initier une merge request à partir de l'issue

## Partie 2 - Initialisation d'un pipeline dans gitlab-ci

1. Créez une nouvelle issue + merge request
2. Ajouter un fichier `.gitlab-ci.yml` à votre projet
3. Faite en sorte que votre fichier gitlab-ci.yml lance un job de compilation
4. Quand la compilation passe, vous devez créer un autre job permettant de lancer les tests unitaires
5. Quand les tests unitaires passent, ajoutez un job, permettant de créer le FatJar

## Partie 3 - Code Quality

1. Créez vous un compte sur [SonarCloud](https://sonarcloud.io/) et créez votre Organization
2. Créez une nouvelle issue + merge request
3. Modifiez votre pom.xml permettant l'utilisation de sonar
4. Modifiez votre pipeline pour que gitlab-ci execute sonar

## Partie 4 - Versionning (Création de Tag)

1. Créez une nouvelle issue + merge request
2. Faite en sorte de pouvoir créer un tag via l'interface de gitlab-ci déclanchant un job manuel qui exécute:  
    a. mvn release:prepare  
    b. mvn release:perform  
4. L'exécutable créé doit-être uploadé sur votre projet GitLab *(cf: Packages & Registries > Package Registry)*
5. Vérfiez la présence du tag *(cf: Repository > Tags)*

## Partie 5 - Dockerized (Création d'une image docker)

1. Créez une nouvelle issue + merge request
2. Lors de la création d'un tag, faites en sorte d'upload une image Docker du FatJar *(cf: Packages & Registries > Container Registry)*
 
*Aide: [Plugin Maven Jib](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin)*

## Partie 6 - Déploiement

1. Créez une nouvelle issue + merge request
2. Faite en sorte que votre application se déploie automatiquement sur Heroku, une fois le tag créé
